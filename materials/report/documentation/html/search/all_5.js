var searchData=
[
  ['piece_15',['Piece',['../classPiece.html',1,'Piece'],['../classPiece.html#ae3a5cfda8e4c7520404f2f4389f4f1c1',1,'Piece::Piece() noexcept=default'],['../classPiece.html#addcd1636e15f296e6f898dde044cacec',1,'Piece::Piece(PieceFigure figure, PieceColor color) noexcept']]],
  ['piececolor_16',['PieceColor',['../classPiece.html#a29912828a54f09def7285afcab17a24f',1,'Piece']]],
  ['piecefigure_17',['PieceFigure',['../classPiece.html#a68f6a601dcf1081562f86da4d4ed79a9',1,'Piece']]]
];
